import React, { useContext, useEffect,useState } from 'react';
import Link from 'next/link';
import { HomeIcon, ClipboardDocumentListIcon, Squares2X2Icon,UserGroupIcon } from '@heroicons/react/24/outline';
import { useRouter } from 'next/router';
import { jwtDecode } from 'jwt-decode';
import { AuthContext } from '@/context/authContext';
let navigationItem = [
  { name: 'Dashboard', href: '/dashboard', icon: HomeIcon, current: false },
  { name: 'Projects', href: '/projects', icon: ClipboardDocumentListIcon, current: false },
  { name: 'Tasks', href: '/tasks', icon: Squares2X2Icon, current: false },
  { name: 'Users', href: '/users', icon: UserGroupIcon, current: false },
];

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(' ');
}

const Sidebar: React.FC = () => {
    const router = useRouter();
    const [navigation, setNavigation] = useState(navigationItem);
    const {user} = useContext(AuthContext)

    useEffect(()=>{

        if(user?.exp < new Date().valueOf()){
          console.log("Token valid")
        } 

        else {
          if(router.pathname != '/login') router.replace("/login")
        }

    },[]);

    const renderNav = ()=>{
        if(user == null ){
            return(
                <div></div>
            )
        }
        else{
            return navigation.map((nav) => (
                <Link key={nav.name} href={nav.href}>
                  <span
                    className={classNames(
                      nav.current
                        ? 'bg-gray-900 text-white'
                        : 'text-gray-300 hover:bg-gray-700 hover:text-white',
                      'group flex items-center px-2 py-2 text-xl font-medium rounded-md'
                    )}
                  >
                    <nav.icon
                      className="mr-3 flex-shrink-0 h-6 w-6 text-gray-400 group-hover:text-gray-300"
                      aria-hidden="true"
                    />
                    {nav.name}
                  </span>
                </Link>
              ))
        }
    }
  


  return (
    <div className="flex flex-col h-full bg-slate-800">
      <div className="flex items-center h-16 flex-shrink-0 px-4 bg-gray-900">
        {/* Your Logo or Branding */}
      </div>
      <div className="flex-1 flex flex-col overflow-y-auto">
        <nav className={`flex-1 px-2 py-4 space-y-1 ${user == false || user == null ? 'hidden':''}`}>
          {renderNav()}
        </nav>
      </div>
    </div>
  );
};

export default Sidebar;
