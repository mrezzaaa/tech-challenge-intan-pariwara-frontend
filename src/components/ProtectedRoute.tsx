"use client";

import { useContext, useEffect, ReactNode } from 'react';
import { useRouter } from 'next/router';
import { AuthContext } from '../context/authContext';

interface ProtectedRouteProps {
  children: ReactNode;
}

const ProtectedRoute = ({ children }: ProtectedRouteProps) => {
  const authContext = useContext(AuthContext);
  const router = useRouter();

  if (!authContext) {
    return null; // or some error handling
  }

  const { user, loading } = authContext;

  useEffect(() => {
    if (!loading && !user) {
      router.push('/login');
    }
  }, [user, loading]);

  if (loading || !user) {
    return <div>Loading...</div>;
  }

  return <>{children}</>;
};

export default ProtectedRoute;
