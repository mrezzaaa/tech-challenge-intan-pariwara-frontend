import React from 'react';
import Sidebar from './Sidebar';
import Header from './Header';

const Layout: React.FC = ({ children }) => {
  return (
    <div className="flex h-screen bg-current" style={{ backgroundColor: 'var(--bg-color)' }}>
      <div className="flex flex-shrink-0">
        <Sidebar />
      </div>
      <div className="flex flex-col w-0 flex-1 overflow-hidden">
        <Header />
        <main className="flex-1 relative overflow-y-auto focus:outline-none min-w-[90vw] bg-current p-4" style={{ backgroundColor: 'var(--bg-color)' }}>
            {children}
        </main>
      </div>
    </div>
  );
};

export default Layout;
