import { useNotification } from '../context/notificationContext';

const Notifications: React.FC = () => {
  const { notifications, removeNotification } = useNotification();

  return (
    <div className="notifications">
      {notifications.map((notification, index) => (
        <div key={index} className="notification">
          {notification}
          <button onClick={() => removeNotification(index)}>Dismiss</button>
        </div>
      ))}
    </div>
  );
};

export default Notifications;
