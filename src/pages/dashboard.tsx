"use client";

import { useState, useEffect } from 'react';
import axios from 'axios';
import { Doughnut } from 'react-chartjs-2';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import ProtectedRoute from '../components/ProtectedRoute';
import { ClipboardDocumentListIcon, BriefcaseIcon, CheckCircleIcon, ClockIcon } from '@heroicons/react/24/outline';

ChartJS.register(ArcElement, Tooltip, Legend);

const DashboardPage = () => {
  const [taskData, setTaskData] = useState({ labels: [], datasets: [] });
  const [projectData, setProjectData] = useState({ labels: [], datasets: [] });
  const [summary, setSummary] = useState({ totalTasks: 0, totalProjects: 0, completedTasks: 0, ongoingProjects: 0 });

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const [tasksResponse, projectsResponse] = await Promise.all([
        axios.get('http://localhost:4000/tasks'),
        axios.get('http://localhost:4000/projects')
      ]);

      const tasks = tasksResponse.data;
      const projects = projectsResponse.data;

      console.log('Fetched tasks:', tasks);
      console.log('Fetched projects:', projects);

      processTaskData(tasks);
      processProjectData(projects);
      calculateSummary(tasks, projects);
      //calculateOnGoing
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const processTaskData = (tasks) => {
    const taskStatuses = {
      'To Do': 0,
      'In Progress': 0,
      'Done': 0,
    };

    tasks.forEach(task => {
      if (taskStatuses[task.status] !== undefined) {
        taskStatuses[task.status]++;
      }
    });

    setTaskData({
      labels: Object.keys(taskStatuses),
      datasets: [{
        data: Object.values(taskStatuses),
        backgroundColor: ['#3B82F6', '#10B981', '#F59E0B'],
        hoverBackgroundColor: ['#2563EB', '#059669', '#D97706']
      }]
    });
  };

  const processProjectData = (projects) => {
    const projectStatuses = {
      'To Do': 0,
      'In Progress': 0,
      'Completed': 0,
    };
  
    projects.forEach(project => {
      if (projectStatuses.hasOwnProperty(project.status)) {
        projectStatuses[project.status]++;
      } else {
        console.warn(`Unexpected project status: ${project.status}`);
      }
      
      // Log tasks for each project
      console.log(`Tasks for project ${project._id}:`, project.tasks);
    });
  
    console.log('Processed project statuses:', projectStatuses);
  
    // if (Object.values(projectStatuses).some(value => value > 0)) {
      setProjectData({
        labels: Object.keys(projectStatuses),
        datasets: [{
          data: Object.values(projectStatuses),
          backgroundColor: ['#8B5CF6', '#EC4899', '#14B8A6', '#F97316'],
          hoverBackgroundColor: ['#7C3AED', '#DB2777', '#0D9488', '#EA580C']
        }]
      });
    // } else {
    //   console.warn('No project data to display');
    //   setProjectData(null);
    // }
  };

  const calculateSummary = (tasks, projects) => {
    setSummary({
      totalTasks: tasks.length,
      totalProjects: projects.length,
      completedTasks: tasks.filter(task => task.status === 'Done').length,
      ongoingProjects: projects.filter(project => project.status === 'In Progress').length
    });
  };

  const SummaryCard = ({ Icon, title, value, color }) => (
    <div className={`bg-white rounded-lg shadow-md p-6 flex items-center ${color}`}>
      <Icon className="h-8 w-8 mr-4" />
      <div>
        <h3 className="text-lg font-semibold">{title}</h3>
        <p className="text-2xl font-bold">{value}</p>
      </div>
    </div>
  );

  return (
    <ProtectedRoute>
      <div className="min-h-screen bg-gray-100 p-8">
        <h1 className="text-3xl font-bold mb-8 text-gray-800">Dashboard</h1>
        
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-6 mb-8">
          <SummaryCard Icon={ClipboardDocumentListIcon} title="Total Tasks" value={summary.totalTasks} color="text-blue-600" />
          <SummaryCard Icon={CheckCircleIcon} title="Completed Tasks" value={summary.completedTasks} color="text-purple-600" />
          <SummaryCard Icon={BriefcaseIcon} title="Total Projects" value={summary.totalProjects} color="text-green-600" />
          <SummaryCard Icon={ClockIcon} title="Ongoing Projects" value={summary.ongoingProjects} color="text-orange-600" />
        </div>

        <div className="grid grid-cols-1 lg:grid-cols-2 gap-8">
          <div className="bg-white p-6 rounded-lg shadow-md">
            <h2 className="text-xl font-semibold mb-4 text-gray-800">Task Status Distribution</h2>
            <div className="h-64">
              <Doughnut data={taskData} options={{ responsive: true, maintainAspectRatio: false }} />
            </div>
          </div>
          <div className="bg-white p-6 rounded-lg shadow-md">
            <h2 className="text-xl font-semibold mb-4 text-gray-800">Project Status Distribution</h2>
            <div className="h-64">
              <Doughnut data={projectData} options={{ responsive: true, maintainAspectRatio: false }} />
            </div>
          </div>
        </div>
      </div>
    </ProtectedRoute>
  );
};

export default DashboardPage;
