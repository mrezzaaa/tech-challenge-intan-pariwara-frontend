module.exports = {
  darkMode: 'media', // or 'class' for toggling dark mode manually
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './src/**/*.{js,ts,jsx,tsx}', // If you have a src directory
  ],
  theme: {
    extend: {},
  },
  plugins: [],
};
