"use client";

import { useState, useEffect, useCallback } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';
import ProtectedRoute from '../../components/ProtectedRoute';

const ProjectDetailsPage = () => {
  const [project, setProject] = useState(null);
  const [loading, setLoading] = useState(true);
  const router = useRouter();
  const { projectId } = router.query;

  useEffect(()=>{
    getDetail()
  },[loading])

  function getDetail(){

    axios.get(`http://localhost:4000/projects/${router.query.id}`)
    .then(response => {
        console.log(response.data)
      setProject(response.data);
      setLoading(false);
    })
    .catch(error => {
      console.error('Error fetching project details:', error);
      setLoading(false);
    });
  }

  if (loading) {
    return (
      <ProtectedRoute>
        <div className="flex justify-center items-center h-64">
          <div className="animate-spin rounded-full h-12 w-12 border-b-2 border-gray-900"></div>
        </div>
      </ProtectedRoute>
    );
  }

  if (!project) {
    return (
      <ProtectedRoute>
        <div className="flex justify-center items-center h-64">
          <p className="text-gray-900 dark:text-white">Project not found</p>
        </div>
      </ProtectedRoute>
    );
  }

  return (
    <ProtectedRoute>
      <div className="min-h-screen p-6">
        <div className="max-w-6xl mx-auto bg-white dark:bg-gray-900 p-8 rounded shadow-md">
          <h1 className="text-3xl font-bold mb-6">{project.name}</h1>
          <p className="text-lg mb-4">{project.description}</p>
          <h2 className="text-2xl font-semibold mb-4">Tasks</h2>
          <ul className="list-disc pl-6 mb-6">
            {project.tasks.map(task => (
              <li key={task._id} className="mb-2">
                <p className="text-gray-800 dark:text-white">{task.name}</p>
                <p className="text-gray-600 dark:text-gray-300">Due: {new Date(task.dueDate).toLocaleDateString()}</p>
                <p className="text-gray-600 dark:text-gray-300">Status: {task.status}</p>
              </li>
            ))}
          </ul>
          <h2 className="text-2xl font-semibold mb-4">Team Members</h2>
          <ul className="list-disc pl-6">
            {project.members.map(member => (
              <li key={member._id} className="mb-2">
                <p className="text-gray-800 dark:text-white">{member.name}</p>
                <p className="text-gray-600 dark:text-gray-300">{member.email}</p>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </ProtectedRoute>
  );
};

export default ProjectDetailsPage;
