"use client";

import { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import ProtectedRoute from '../components/ProtectedRoute';
import {AuthContext} from '../context/authContext'; // Assuming you have an AuthContext
import { jwtDecode } from 'jwt-decode';

const ProjectsPage = () => {
  const [projects, setProjects] = useState([]);
  const [loading, setLoading] = useState(true);
  const [searchQuery, setSearchQuery] = useState('');
  const [filterStatus, setFilterStatus] = useState('');
  const router = useRouter();
  const [ user,setUser ] = useState(null)

  
  useEffect(() => {
    fetchProjects();
  }, []);

  const fetchProjects = () => {
    let token = localStorage.getItem("token")
    if(token){
      let decoded = jwtDecode(token)
      setUser(decoded)
    }
    const url = 'http://localhost:4000/projects' 


    axios.get(url)
      .then(response => {
        setProjects(response.data);
        setLoading(false);
      })
      .catch(error => {
        console.error('Error fetching projects:', error);
        setLoading(false);
      });
  };

  const handleAddProject = () => {
    router.push('/projects/add');
  };

  const handleSearchChange = (e) => {
    setSearchQuery(e.target.value);
  };

  const handleFilterChange = (e) => {
    setFilterStatus(e.target.value);
  };

  const filteredProjects = projects.filter(project => {
    return (
      project.name.toLowerCase().includes(searchQuery.toLowerCase()) ||
      project.description.toLowerCase().includes(searchQuery.toLowerCase())
    ) && (filterStatus ? project.status === filterStatus : true);
  });

  const handleProjectClick = (projectId) => {
    console.log(`Opening /projects/${projectId}`)
    router.push(`/projects/${projectId}`);
  };

  return (
    <ProtectedRoute>
      <div className="min-h-screen p-6">
        <div className="max-w-6xl mx-auto">
          <div className="flex justify-between items-center mb-6">
            <h1 className="text-3xl font-bold">Projects</h1>
            {user?.role === 'Admin' && (
              <button 
                onClick={handleAddProject}
                className="px-4 py-2 bg-blue-500 rounded hover:bg-blue-600 transition duration-300 ease-in-out focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-opacity-50"
              >
                Add Project
              </button>
            )}
          </div>
          <div className="flex space-x-4 mb-6">
            <input
              type="text"
              placeholder="Search..."
              value={searchQuery}
              onChange={handleSearchChange}
              className="dark:bg-slate-900 px-4 py-2 border rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-blue-500"
            />
            <select
              value={filterStatus}
              onChange={handleFilterChange}
              className="dark:bg-slate-900 px-4 py-2 border rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-blue-500"
            >
              <option value="">All Statuses</option>
              <option value="Active">Active</option>
              <option value="Completed">Completed</option>
            </select>
          </div>
          {loading ? (
            <div className="flex justify-center items-center h-64">
              <div className="animate-spin rounded-full h-12 w-12 border-b-2 border-gray-900"></div>
            </div>
          ) : (
            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
              {filteredProjects.map((project: any) => (
                <div 
                  key={project._id}
                  className="bg-white dark:bg-gray-900 p-4 rounded shadow-md hover:shadow-lg transition duration-300 ease-in-out transform hover:-translate-y-1 cursor-pointer"
                  onClick={() => handleProjectClick(project._id)}
                >
                  <h3 className="font-semibold text-lg">{project.name}</h3>
                  <div className="mt-2 text-sm text-gray-600">
                    <p>Description: {project.description}</p>
                    <p>Tasks: {project.tasks.length || 0}</p>
                    <p>Status: {project.status || 'Active'}</p>
                  </div>
                </div>
              ))}
            </div>
          )}
        </div>
      </div>
    </ProtectedRoute>
  );
};

export default ProjectsPage;
