"use client";

import { createContext, useState, useEffect, ReactNode, useCallback } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';
import {jwtDecode} from 'jwt-decode';

interface AuthContextType {
  user: any;
  login: (username: string, password: string) => Promise<void>;
  logout: () => void;
  loading: boolean;
}

const AuthContext = createContext<AuthContextType | undefined>(undefined);

interface AuthProviderProps {
  children: ReactNode;
}

export const AuthProvider = ({ children }: AuthProviderProps) => {
  const [user, setUser] = useState<any>(null);
  const [loading, setLoading] = useState(true);
  const router = useRouter();

  useEffect(() => {

    if ( (user?.exp * 1000) < new Date().valueOf() ) {
      const decodedUser = jwtDecode(token);
      const expired = decodedUser.exp * 1000
      if(expired < new Date().valueOf()){
        router.replace("/login")
      }
      setUser(decodedUser);
    }
    else{
        router.replace("/login")
    }
    setLoading(false);
  }, []);

  const login = useCallback(async (username: string, password: string) => {
    const response = await axios.post('http://localhost:4000/auth/login', { username, password }).then((response)=>{
        const { access_token } = response.data;
        localStorage.setItem('token', access_token);
        const decodedUser = jwtDecode(access_token);
        setUser(decodedUser);
        router.push('/dashboard');
    }).catch((er)=>{
        console.log(er)
        if(er.message.includes('401')){
          alert("User not found")
        }
        else{
          alert(er.message)
        }
    });
  }, [router]);

  const logout = useCallback(() => {
    localStorage.removeItem('token');
    setUser(null);
    router.push('/login');
  }, [router]);

  return (
    <AuthContext.Provider value={{ user, login, logout, loading }}>
      {children}
    </AuthContext.Provider>
  );
};

export { AuthContext };
