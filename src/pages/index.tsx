"use client";

import { useContext, useEffect } from 'react';
import { useRouter } from 'next/router';
import  {AuthContext}  from '../context/authContext';
import Link from 'next/link';

const HomePage: React.FC = () => {
  const authContext = useContext(AuthContext);
  const router = useRouter();

  useEffect(() => {
    if (!authContext?.user && !authContext?.loading) {
      router.push('/login');
    }
  }, [authContext?.user, authContext?.loading]);

  if (authContext?.loading || !authContext?.user) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1>Welcome to the Home Page</h1>
      <p>This is a protected route. Only logged-in users can see this.</p>
      <Link href="/dashboard">Go to Dashboard</Link>
    </div>
  );
};

export default HomePage;
