"use client";

import { useState, useEffect } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import ProtectedRoute from '../../components/ProtectedRoute';

const AddTaskPage = () => {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [project, setProject] = useState('');
  const [assignedTo, setAssignedTo] = useState('');
  const [dueDate, setDueDate] = useState('');
  const [projects, setProjects] = useState([]);
  const [users, setUsers] = useState([]);
  const router = useRouter();

  useEffect(() => {
    const token = localStorage.getItem('token');
    const fetchData = async () => {
      try {
        const [projectsResponse, usersResponse] = await Promise.all([
          axios.get('http://localhost:4000/projects'),
          axios.get('http://localhost:4000/users', {
            headers: { "Authorization": "Bearer " + token }
          })
        ]);
        setProjects(projectsResponse.data);
        setUsers(usersResponse.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await axios.post('http://localhost:4000/tasks', { name, description, project, assignedTo, dueDate, status: 'To Do' });
      router.push('/tasks');
    } catch (error) {
      console.error('Error adding task:', error);
    }
  };

  return (
    <ProtectedRoute>
      <div className="min-h-screen bg-gray-100 py-6 flex flex-col justify-center sm:py-12">
        <div className="relative py-3 sm:max-w-xl sm:mx-auto">
          <div className="absolute inset-0 bg-gradient-to-r from-cyan-400 to-light-blue-500 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl"></div>
          <div className="relative px-4 py-10 bg-white shadow-lg sm:rounded-3xl sm:p-20">
            <div className="max-w-md mx-auto">
              <div>
                <h1 className="text-2xl font-semibold text-center">Add New Task</h1>
              </div>
              <div className="divide-y divide-gray-200">
                <form onSubmit={handleSubmit} className="py-8 text-base leading-6 space-y-4 text-gray-700 sm:text-lg sm:leading-7">
                  <div className="relative">
                    <input 
                      type="text" 
                      id="name" 
                      name="name" 
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                      className="peer h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:border-rose-600" 
                      placeholder=" " 
                      required
                    />
                    <label htmlFor="name" className="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm">Task Name</label>
                  </div>
                  <div className="relative">
                    <textarea 
                      id="description" 
                      name="description" 
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                      className="peer h-20 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:border-rose-600" 
                      placeholder=" " 
                      required
                    ></textarea>
                    <label htmlFor="description" className="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm">Description</label>
                  </div>
                  <div className="relative">
                    <select 
                      id="project" 
                      name="project" 
                      value={project}
                      onChange={(e) => setProject(e.target.value)}
                      className="peer h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:border-rose-600" 
                      required
                    >
                      <option value="">Select Project</option>
                      {projects.map(project => (
                        <option key={project._id} value={project._id}>{project.name}</option>
                      ))}
                    </select>
                    <label htmlFor="project" className="absolute left-0 -top-3.5 text-gray-600 text-sm">Project</label>
                  </div>
                  <div className="relative">
                    <select 
                      id="assignedTo" 
                      name="assignedTo" 
                      value={assignedTo}
                      onChange={(e) => setAssignedTo(e.target.value)}
                      className="peer h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:border-rose-600" 
                      required
                    >
                      <option value="">Select User</option>
                      {users.map(user => (
                        <option key={user._id} value={user._id}>{user.username}</option>
                      ))}
                    </select>
                    <label htmlFor="assignedTo" className="absolute left-0 -top-3.5 text-gray-600 text-sm">Assign To</label>
                  </div>
                  <div className="relative">
                    <input 
                      type="date" 
                      id="dueDate" 
                      name="dueDate" 
                      value={dueDate}
                      onChange={(e) => setDueDate(e.target.value)}
                      className="peer h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:border-rose-600" 
                      required
                    />
                    <label htmlFor="dueDate" className="absolute left-0 -top-3.5 text-gray-600 text-sm">Due Date</label>
                  </div>
                  <div className="relative">
                    <button type="submit" className="bg-blue-500 text-white rounded-md px-4 py-2 hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:ring-opacity-50">Add Task</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </ProtectedRoute>
  );
};

export default AddTaskPage;