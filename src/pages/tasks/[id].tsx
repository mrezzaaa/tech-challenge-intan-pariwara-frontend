"use client";

import { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import ProtectedRoute from '../../components/ProtectedRoute';
import {AuthContext} from '../../context/authContext'; // Assuming you have an AuthContext

const TaskDetailsPage = () => {
  const [task, setTask] = useState(null);
  const [loading, setLoading] = useState(true);
  const [isEditing, setIsEditing] = useState(false);
  const [formValues, setFormValues] = useState({
    name: '',
    description: '',
    dueDate: '',
    status: '',
  });
  const router = useRouter();
  const  taskId = router.query.id;
  const { user } = useContext(AuthContext);

  useEffect(() => {
    if (taskId) {
      fetchTaskDetails();
    }
  }, [taskId]);

  const fetchTaskDetails = async () => {
    try {
      const response = await axios.get(`http://localhost:4000/tasks/${taskId}`);
      setTask(response.data);
      setFormValues({
        name: response.data.name,
        description: response.data.description,
        dueDate: new Date(response.data.dueDate).toISOString().split('T')[0],
        status: response.data.status,
      });
      setLoading(false);
    } catch (error) {
      console.error('Error fetching task details:', error);
      setLoading(false);
    }
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    try {
      await axios.patch(`http://localhost:4000/tasks/${taskId}`, formValues);
      setIsEditing(false);
      fetchTaskDetails();
    } catch (error) {
      console.error('Error updating task:', error);
    }
  };

  const toggleEdit = () => {
    setIsEditing(!isEditing);
  };

  if (loading) {
    return (
      <ProtectedRoute>
        <div className="flex justify-center items-center h-64">
          <div className="animate-spin rounded-full h-12 w-12 border-b-2 border-gray-900"></div>
        </div>
      </ProtectedRoute>
    );
  }

  if (!task) {
    return (
      <ProtectedRoute>
        <div className="flex justify-center items-center h-64">
          <p className="text-gray-900 dark:text-white">Task not found</p>
        </div>
      </ProtectedRoute>
    );
  }

  return (
    <ProtectedRoute>
      <div className="min-h-screen p-6">
        <div className="max-w-6xl mx-auto bg-white dark:bg-gray-900 p-8 rounded shadow-md">
          <h1 className="text-3xl font-bold mb-6">Task Details</h1>
          {isEditing ? (
            <form onSubmit={handleFormSubmit} className="space-y-6">
              <div>
                <label className="block text-sm font-medium text-gray-700 dark:text-gray-300">Name</label>
                <input
                  type="text"
                  name="name"
                  value={formValues.name}
                  onChange={handleInputChange}
                  className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                />
              </div>
              <div>
                <label className="block text-sm font-medium text-gray-700 dark:text-gray-300">Description</label>
                <textarea
                  name="description"
                  value={formValues.description}
                  onChange={handleInputChange}
                  className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                ></textarea>
              </div>
              <div>
                <label className="block text-sm font-medium text-gray-700 dark:text-gray-300">Due Date</label>
                <input
                  type="date"
                  name="dueDate"
                  value={formValues.dueDate}
                  onChange={handleInputChange}
                  className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                />
              </div>
              <div>
                <label className="block text-sm font-medium text-gray-700 dark:text-gray-300">Status</label>
                <select
                  name="status"
                  value={formValues.status}
                  onChange={handleInputChange}
                  className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                >
                  <option value="Pending">Pending</option>
                  <option value="In Progress">In Progress</option>
                  <option value="Done">Done</option>
                </select>
              </div>
              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={toggleEdit}
                  className="px-4 py-2 mr-2 bg-gray-300 rounded hover:bg-gray-400 transition duration-300 ease-in-out focus:outline-none focus:ring-2 focus:ring-gray-500"
                >
                  Cancel
                </button>
                <button
                  type="submit"
                  className="px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600 transition duration-300 ease-in-out focus:outline-none focus:ring-2 focus:ring-blue-500"
                >
                  Save
                </button>
              </div>
            </form>
          ) : (
            <div>
              <h2 className="text-2xl font-semibold mb-4">{task.name}</h2>
              <p className="text-lg mb-4">{task.description}</p>
              <p className="text-md mb-2">Due Date: {new Date(task.dueDate).toLocaleDateString()}</p>
              <p className="text-md mb-2">Status: {task.status}</p>
              {user?.role === 'admin' || user?.role === 'teamMember' ? (
                <button
                  onClick={toggleEdit}
                  className="px-4 py-2 bg-indigo-600 text-white rounded hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-opacity-50"
                >
                  Edit Task
                </button>
              ) : null}
            </div>
          )}
        </div>
      </div>
    </ProtectedRoute>
  );
};

export default TaskDetailsPage;
