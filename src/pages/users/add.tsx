"use client";

import { useState } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import ProtectedRoute from '../../components/ProtectedRoute';

const AddUserPage: React.FC = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [role, setRole] = useState('TeamMember');
  const router = useRouter();

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      await axios.post('http://localhost:4000/users', { username, password, role });
      router.push('/users');
    } catch (error) {
      console.error('Error adding user:', error);
    }
  };

  return (
    <ProtectedRoute>
      <div>
        <h1>Add User</h1>
        <form onSubmit={handleSubmit}>
          <div>
            <label>Username</label>
            <input
              type="text"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              required
            />
          </div>
          <div>
            <label>Password</label>
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </div>
          <div>
            <label>Role</label>
            <select value={role} onChange={(e) => setRole(e.target.value)} required>
              <option value="Admin">Admin</option>
              <option value="TeamMember">TeamMember</option>
            </select>
          </div>
          <button type="submit">Add User</button>
        </form>
      </div>
    </ProtectedRoute>
  );
};

export default AddUserPage;
