"use client";

import { useState, useEffect } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import ProtectedRoute from '../components/ProtectedRoute';
import { useSocket } from '../context/socketContext';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

const TasksPage = () => {
  const [tasks, setTasks] = useState([]);
  const [loading, setLoading] = useState(true);
  const [searchQuery, setSearchQuery] = useState('');
  const [filterPriority, setFilterPriority] = useState('');
  const router = useRouter();
  const socket = useSocket();

  useEffect(() => {
    fetchTasks();

    if (socket) {
      socket.on('taskCreated', (task) => {
        setTasks((prevTasks) => [...prevTasks, task]);
      });

      socket.on('taskUpdated', (updatedTask) => {
        setTasks((prevTasks) =>
          prevTasks.map((task) => (task._id === updatedTask._id ? updatedTask : task))
        );
      });

      socket.on('taskDeleted', (deletedTask) => {
        setTasks((prevTasks) =>
          prevTasks.filter((task) => task._id !== deletedTask._id)
        );
      });
    }

    return () => {
      if (socket) {
        socket.off('taskCreated');
        socket.off('taskUpdated');
        socket.off('taskDeleted');
      }
    };
  }, [socket]);

  const fetchTasks = () => {
    axios.get('http://localhost:4000/tasks')
      .then(response => {
        console.log(response.data)
        setTasks(response.data);
        setLoading(false);
      })
      .catch(error => {
        console.error('Error fetching tasks:', error);
        setLoading(false);
      });
  };

  const handleAddTask = () => {
    router.push('/tasks/add');
  };

  const handleSearchChange = (e) => {
    setSearchQuery(e.target.value);
  };

  const handleFilterChange = (e) => {
    setFilterPriority(e.target.value);
  };

  const getTasksByStatus = (status) => {
    return tasks.filter(task => {
      const taskStatus = task.status.toLowerCase();
      return taskStatus === status.toLowerCase() ||
        (status === 'To Do' && taskStatus === 'To Do') ||
        (status === 'In Progress' && (taskStatus === 'In Progress' || taskStatus === 'in-progress')) ||
        (status === 'Done' && taskStatus === 'Completed');
    });
  };

  const filteredTasks = tasks.filter(task => {
    return (
      task.name.toLowerCase().includes(searchQuery.toLowerCase()) ||
      task.description.toLowerCase().includes(searchQuery.toLowerCase())
    ) && (filterPriority ? task.priority === filterPriority : true);
  });

  const renderTaskCard = (task, index) => (
    <Draggable key={task._id} draggableId={task._id} index={index} >
      {(provided) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          className="bg-white dark:bg-gray-700 p-4 rounded-lg shadow mb-2"
          onClick={()=>{router.push(`/tasks/${task._id}`)}}
        >
          <h3 className="font-semibold text-gray-800 dark:text-white">{task.name}</h3>
          <p className="text-sm text-gray-600 dark:text-gray-300 mt-1">Due: {new Date(task.dueDate).toLocaleDateString()}</p>
          <p className="text-xs text-gray-500 dark:text-gray-400 mt-1">Status: {task.status}</p>
        </div>
      )}
    </Draggable>
  );

  const columns = ['To Do', 'In Progress', 'Done'];

  const onDragEnd = (result) => {
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    const task = tasks.find(t => t._id === draggableId);
    const newStatus = destination.droppableId;

    // Update task status in the backend
    axios.patch(`http://localhost:4000/tasks/${task._id}`, { ...task, status: newStatus })
      .then(response => {
        // Update local state
        setTasks(prevTasks => 
          prevTasks.map(t => 
            t._id === task._id ? { ...t, status: newStatus } : t
          )
        );
      })
      .catch(error => {
        console.error('Error updating task status:', error);
      });
  };

  return (
    <ProtectedRoute>
      <div className="min-h-screen 0 dark:bg-gray-900 p-6">
        <div className="max-w-7xl mx-auto">
          <div className="flex justify-between items-center mb-6">
            <h1 className="text-3xl font-bold text-gray-900 dark:text-white">Tasks</h1>
            <button
              onClick={handleAddTask}
              className="px-4 py-2 bg-teal-600 text-white rounded hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-opacity-50"
            >
              Add Task
            </button>
          </div>
          
          <div className="flex space-x-4 mb-6">
            <input
              type="text"
              placeholder="Search..."
              value={searchQuery}
              onChange={handleSearchChange}
              className="dark:bg-slate-900 px-4 py-2 border rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-indigo-500"
            />
            <select
              value={filterPriority}
              onChange={handleFilterChange}
              className="dark:bg-slate-900 px-4 py-2 border rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-indigo-500"
            >
              <option value="">All Priorities</option>
              <option value="High">High</option>
              <option value="Medium">Medium</option>
              <option value="Low">Low</option>
            </select>
          </div>

          {loading ? (
            <div className="flex justify-center items-center h-64">
              <div className="animate-spin rounded-full h-12 w-12 border-b-2 border-indigo-500"></div>
            </div>
          ) : (
            <DragDropContext onDragEnd={onDragEnd}>
              <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
                {columns.map(status => (
                  <Droppable key={status} droppableId={status}>
                    {(provided) => (
                      <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        className=" dark:bg-gray-800 p-4 rounded-lg"
                      >
                        <h2 className="text-xl font-semibold mb-4 text-gray-800 dark:text-white">{status}</h2>
                        <div className="space-y-2">
                          {filteredTasks.filter(task => task.status === status).map((task, index) => renderTaskCard(task, index))}
                        </div>
                        {provided.placeholder}
                      </div>
                    )}
                  </Droppable>
                ))}
              </div>
            </DragDropContext>
          )}
        </div>
      </div>
    </ProtectedRoute>
  );
};

export default TasksPage;
