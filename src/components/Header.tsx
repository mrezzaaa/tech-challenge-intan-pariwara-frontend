import React, { useContext, useEffect, useState } from 'react';
import { BellIcon, UserCircleIcon } from '@heroicons/react/24/outline';
import { Router, useRouter } from 'next/router';
import { jwtDecode } from 'jwt-decode';
import { AuthContext } from '@/context/authContext';

const Header: React.FC = () => {
    const router = useRouter()
    const {user} = useContext(AuthContext)

    useEffect(()=>{
      
    },[user])

    



    function logout(){
        localStorage.setItem("token","")
        window.location.reload()
        router.replace("/login")
    }
    console.log("Header",user)
    if(user?.exp * 1000 < new Date().valueOf() || user == null){
      return(
        <div></div>
      )
    }
    else{
      return (
        <div className="flex justify-between items-center h-16 bg-gray-900 px-4 sm:px-6 lg:px-8">
          <div className="flex items-center">
            
            <div className='justify-start'>
              <h1>{user?.username} - {user?.role}</h1>
            </div>
          </div>
          <div className="flex  w-[10%] items-center justify-between">
            <button className="text-gray-300 hover:text-white focus:outline-none focus:text-white">
              <span className="sr-only">View notifications</span>
              <BellIcon className="h-6 w-6" aria-hidden="true" />
            </button>
            <button className="text-gray-300 hover:text-white items-center text-center focus:outline-none focus:text-white" onClick={()=>{logout()}}>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="size-6">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M8.25 9V5.25A2.25 2.25 0 0 1 10.5 3h6a2.25 2.25 0 0 1 2.25 2.25v13.5A2.25 2.25 0 0 1 16.5 21h-6a2.25 2.25 0 0 1-2.25-2.25V15m-3 0-3-3m0 0 3-3m-3 3H15" />
                </svg>
            </button>
            <button className="text-gray-300 hover:text-white focus:outline-none focus:text-white">
              <span className="sr-only">User menu</span>
              <UserCircleIcon className="h-6 w-6" aria-hidden="true" />
            </button>
          </div>
        </div>
      );

    }
};

export default Header;
