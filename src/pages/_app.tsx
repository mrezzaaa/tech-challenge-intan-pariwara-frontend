import "../app/globals.css"
import { AppProps } from 'next/app';
import Layout from '../components/Layout';
import  {AuthProvider}  from '../context/authContext';
import { SocketProvider } from '../context/socketContext';
import { NotificationProvider } from '../context/notificationContext';
import Notifications from '../components/Notifications';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <AuthProvider>
      <SocketProvider>
        <NotificationProvider>
          <Layout>
            <Component {...pageProps} />
            <Notifications />
          </Layout>
        </NotificationProvider>
      </SocketProvider>
    </AuthProvider>
  );
}

export default MyApp;
