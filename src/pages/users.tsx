"use client";

import { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import ProtectedRoute from '../components/ProtectedRoute';
import {AuthContext} from '../context/authContext'; // Assuming you have an AuthContext

const UsersPage: React.FC = () => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true);
  const [formValues, setFormValues] = useState({ username: '', password: '', role: 'TeamMember' });
  const [isEditing, setIsEditing] = useState(false);
  const [editUserId, setEditUserId] = useState(null);
  const router = useRouter();
  const { user } = useContext(AuthContext);

  useEffect(() => {
    const token = localStorage.getItem('token');
    axios.get('http://localhost:4000/users', { headers: { "Authorization": "Bearer " + token } })
      .then(response => {
        setUsers(response.data);
        setLoading(false);
      })
      .catch(error => {
        console.error('Error fetching users:', error);
        setLoading(false);
      });
  }, []);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    const token = localStorage.getItem('token');
    try {
      if (isEditing) {
        await axios.patch(`http://localhost:4000/users/${editUserId}`, formValues, {
          headers: { "Authorization": "Bearer " + token }
        });
      } else {
        console.log("Submitting",formValues)
        await axios.post('http://localhost:4000/users', formValues, {
          headers: { "Authorization": "Bearer " + token }
        });
      }
      setIsEditing(false);
      setFormValues({ username: '', password: '', role: 'teamMember' });
      fetchUsers();
    } catch (error) {
      console.error('Error saving user:', error);
    }
  };

  const handleEditUser = (user) => {
    setEditUserId(user._id);
    setFormValues({ username: user.username, password: user.password, role: user.role });
    setIsEditing(true);
  };

  const handleDeleteUser = async (userId) => {
    const token = localStorage.getItem('token');
    try {
      await axios.delete(`http://localhost:4000/users/${userId}`, {
        headers: { "Authorization": "Bearer " + token }
      });
      fetchUsers();
    } catch (error) {
      console.error('Error deleting user:', error);
    }
  };

  const handleCancelEdit = () => {
    setIsEditing(false);
    setFormValues({ username: '', password: '', role: 'teamMember' });
  };

  const fetchUsers = async () => {
    const token = localStorage.getItem('token');
    axios.get('http://localhost:4000/users', { headers: { "Authorization": "Bearer " + token } })
      .then(response => {
        setUsers(response.data);
        setLoading(false);
      })
      .catch(error => {
        console.error('Error fetching users:', error);
        setLoading(false);
      });
  };

  return (
    <ProtectedRoute>
      <div className="min-h-screen p-6">
        <div className="max-w-6xl mx-auto">
          <h1 className="text-3xl font-bold mb-6">User Management</h1>
          {loading ? (
            <div className="flex justify-center items-center h-64">
              <div className="animate-spin rounded-full h-12 w-12 border-b-2 border-gray-900"></div>
            </div>
          ) : (
            <>
              <form onSubmit={handleFormSubmit} className="space-y-6 mb-6  p-4 rounded shadow-md">
                <div>
                  <label className="block text-sm font-medium ">Username</label>
                  <input
                    type="text"
                    name="username"
                    value={formValues.username}
                    onChange={handleInputChange}
                    required
                    className="dark:bg-slate-900 mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>
                <div>
                  <label className="block text-sm font-medium ">Password</label>
                  <input
                    type="password"
                    name="password"
                    value={formValues.password}
                    onChange={handleInputChange}
                    required
                    className="dark:bg-slate-900 mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>
                <div>
                  <label className="block text-sm font-medium ">Role</label>
                  <select
                    name="role"
                    value={formValues.role}
                    onChange={handleInputChange}
                    required
                    className="dark:bg-slate-900 mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  >
                    <option value="Admin">Admin</option>
                    <option value="TeamMember">Team Member</option>
                  </select>
                </div>
                <div className="flex justify-end">
                  {isEditing && (
                    <button
                      type="button"
                      onClick={handleCancelEdit}
                      className="px-4 py-2 mr-2 rounded hover:bg-gray-400 transition duration-300 ease-in-out focus:outline-none focus:ring-2 focus:ring-gray-500"
                    >
                      Cancel
                    </button>
                  )}
                  <button
                    type="submit"
                    className="px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600 transition duration-300 ease-in-out focus:outline-none focus:ring-2 focus:ring-blue-500"
                  >
                    {isEditing ? 'Update User' : 'Add User'}
                  </button>
                </div>
              </form>
              <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
                {users.map((user) => (
                  <div 
                    key={user._id}
                    className=" dark:bg-gray-900 p-4 rounded shadow-md hover:shadow-lg transition duration-300 ease-in-out transform hover:-translate-y-1"
                  >
                    <h3 className="font-semibold text-lg">{user.name}</h3>
                    <div className="mt-2 text-sm text-gray-600">
                      <p>Username: {user.username}</p>
                      <p>Role: {user.role}</p>
                    </div>
                    <div className="flex justify-end space-x-2 mt-4">
                      <button
                        onClick={() => handleEditUser(user)}
                        className="px-4 py-2 bg-yellow-500 text-white rounded hover:bg-yellow-600 transition duration-300 ease-in-out focus:outline-none focus:ring-2 focus:ring-yellow-500"
                      >
                        Edit
                      </button>
                      <button
                        onClick={() => handleDeleteUser(user._id)}
                        className="px-4 py-2 bg-red-500 text-white rounded hover:bg-red-600 transition duration-300 ease-in-out focus:outline-none focus:ring-2 focus:ring-red-500"
                      >
                        Delete
                      </button>
                    </div>
                  </div>
                ))}
              </div>
            </>
          )}
        </div>
      </div>
    </ProtectedRoute>
  );
};

export default UsersPage;
