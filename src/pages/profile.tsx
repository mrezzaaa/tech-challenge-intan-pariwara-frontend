"use client";

import { useState, useContext, useEffect } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import ProtectedRoute from '../components/ProtectedRoute';
import { AuthContext } from '../context/authContext';

const ProfilePage: React.FC = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [role, setRole] = useState('');
  const router = useRouter();
  const authContext = useContext(AuthContext);

  useEffect(() => {
    const token = localStorage.getItem('token');
    axios.get('http://localhost:4000/users/me', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then(response => {
      const user = response.data;
      setUsername(user.username);
      setRole(user.role);
    })
    .catch(error => {
      console.error('Error fetching profile:', error);
    });
  }, []);

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    const token = localStorage.getItem('token');
    try {
      await axios.patch('http://localhost:4000/users/me', { password }, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      router.push('/dashboard');
    } catch (error) {
      console.error('Error updating profile:', error);
    }
  };

  return (
    <ProtectedRoute>
      <div>
        <h1>Profile</h1>
        <form onSubmit={handleSubmit}>
          <div>
            <label>Username</label>
            <input type="text" value={username} readOnly />
          </div>
          <div>
            <label>Role</label>
            <input type="text" value={role} readOnly />
          </div>
          <div>
            <label>New Password</label>
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </div>
          <button type="submit">Update Profile</button>
        </form>
      </div>
    </ProtectedRoute>
  );
};

export default ProfilePage;
